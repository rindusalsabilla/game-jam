# Game Jam

Her name is Lora. We need to help Lora to find her Treasure. Treasure Finder is a prototype adventure game in which the player controls Lora's movement to find the treasure. The treasure can be anywhere. But be careful!! Just like a human being Lora only has one precious life.  It won't be easy, climb the mountain and down the valley is the best way to get the treasure. Good Luck!​

This game contains four different level that has obstacles in every level. Lora can collect coins while she find her treasure.

How to play??
It's very easy and simple, just use your up pressed button on your keyboard to jump, left pressed button to go left and right pressed button to go right.

Link for the source code:
https://gitlab.com/rindusalsabilla/game-jam​

Source Assets:
https://www.gameart2d.com/freebies.html

https://kenney.nl/

https://pngtree.com/